1) Les question:
	Est ce que il y a des relations entre la qualité du vin et (acidity, volatile, citric acid, residual sugar, alcohol) ? 
	ou simplement la relations entre un attribut(par exemple acidity) et la qualité;
	si oui, ce sont quelle types de relation ?
2) Sur les données:

	"This datasets are related to red variants of the Portuguese "Vinho Verde" wine. The goal is to model wine quality based on physicochemical tests. Due to privacy and logistic issues, only physicochemical (inputs) and sensory (the output) variables are available (e.g. there is no data about grape types, wine brand, wine selling price, etc.)
	"These datasets can be viewed as classification or regression tasks."

	Input variables (based on physicochemical tests): 
		1 - fixed acidity 
		2 - volatile acidity 
		3 - citric acid 
		4 - residual sugar 
		5 - chlorides 
		6 - free sulfur dioxide 
		7 - total sulfur dioxide 
		8 - density 
		9 - pH 
		10 - sulphates 
		11 - alcohol 
	Output variable (based on sensory data): 
		12 - quality (score between 0 and 10)

3) Méthode d’analyse et les visualisations
	statistic simple et description des données.
	visualisation des données en utilisant les diagrammes qu'on a vue dans les cours.
	linéaire regression : qualité = a*acidity + b*volatile + c*acid + d* residual sugar ... 
	(On n'a pas décidé les attribtuts qu'on vas choisir)

4) LI Yi et Michel-abel Muwala
	git : Yi Li